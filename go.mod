module selenium

go 1.20

require (
	github.com/PuerkitoBio/goquery v1.8.1
	github.com/go-chi/chi v1.5.4
	github.com/rs/zerolog v1.29.1
	github.com/swaggo/http-swagger v1.3.4
	github.com/swaggo/swag v1.16.1
	github.com/tebeka/selenium v0.9.9
	gitlab.com/mikhail-sukhodolov/go-kata v0.0.0-20230613124108-2d7d8279ca16
)

require (
	github.com/KyleBanks/depth v1.2.1 // indirect
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/go-openapi/jsonpointer v0.19.6 // indirect
	github.com/go-openapi/jsonreference v0.20.2 // indirect
	github.com/go-openapi/spec v0.20.9 // indirect
	github.com/go-openapi/swag v0.22.4 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/swaggo/files v1.0.1 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sys v0.9.0 // indirect
	golang.org/x/tools v0.9.3 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
