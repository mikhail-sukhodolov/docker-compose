package driver

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
	"github.com/tebeka/selenium/firefox"
)

const maxTries = 5

func RunFirefoxDriver() (selenium.WebDriver, error) {
	// прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "firefox",
	}

	// добавляем в конфигурацию драйвера настройки для chrome
	ffCaps := firefox.Capabilities{}
	caps.AddFirefox(ffCaps)

	// переменная нашего веб-драйвера
	var wd selenium.WebDriver

	// прописываем адрес нашего драйвера
	// В коде драйвера, urlPrefix := "http://selenium:4444/wd/hub" указывает на то, что веб-драйвер должен обращаться
	// к хосту selenium на порту 4444 для установления соединения с Selenium WebDriver.

	// В данном случае, внутри сети network1 сервис selenium доступен по имени контейнера "selenium". Docker Compose
	// автоматически предоставляет встроенный DNS-сервер, который позволяет обращаться к контейнерам по их именам внутри сети.
	// Таким образом, используя http://selenium:4444/wd/hub, драйвер может установить соединение с контейнером selenium,
	// не зависимо от его конкретного IP-адреса.
	urlPrefix := "http://selenium:4444/wd/hub"

	// немного костылей, чтобы драйвер не падал
	for i := 1; i <= maxTries; i++ {
		var err error
		wd, err = selenium.NewRemote(caps, urlPrefix)
		if err != nil {
			log.Fatal().Msg(err.Error())
			continue
		}
		return wd, nil
	}

	return nil, fmt.Errorf("failed to create and configure web driver")
}

func RunChromeDriver() (selenium.WebDriver, error) {
	// прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "chrome",
	}

	// добавляем в конфигурацию драйвера настройки для chrome
	chrCaps := chrome.Capabilities{
		W3C: true,
	}
	caps.AddChrome(chrCaps)

	// переменная нашего веб-драйвера
	var wd selenium.WebDriver

	// прописываем адрес нашего драйвера
	urlPrefix := "http://selenium:4444/wd/hub"

	// немного костылей, чтобы драйвер не падал
	for i := 1; i <= maxTries; i++ {
		var err error
		wd, err = selenium.NewRemote(caps, urlPrefix)
		if err != nil {
			log.Fatal().Msg(err.Error())
			continue
		}
		return wd, nil
	}

	return nil, fmt.Errorf("failed to create and configure web driver")
}
