package service

import (
	"selenium/internal/app/model"
	"selenium/internal/app/repository"
)

type IService interface {
	NewVacancy(vacancy model.Vacancy) error
	GetVacancyByID(id int) (model.Vacancy, error)
	GetVacancyList() ([]model.Vacancy, error)
	DeleteVacancy(id int) error
}

type Service struct {
	repo repository.IRepository
}

func NewService() *Service {
	return &Service{repo: repository.NewRepository()}
}

func (s *Service) NewVacancy(vacancy model.Vacancy) error {
	return s.repo.AddVacancy(vacancy)
}

func (s *Service) GetVacancyByID(id int) (model.Vacancy, error) {
	return s.repo.GetByID(id)
}

func (s *Service) GetVacancyList() ([]model.Vacancy, error) {
	return s.repo.GetList()
}

func (s *Service) DeleteVacancy(id int) error {
	return s.repo.Delete(id)
}
