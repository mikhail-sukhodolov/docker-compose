package repository

import (
	"errors"
	"selenium/internal/app/model"
	"sync"
)

type IRepository interface {
	AddVacancy(vacancy model.Vacancy) error
	GetByID(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}

type Repository struct {
	data  map[string]*model.Vacancy
	count int
	sync.RWMutex
}

func NewRepository() *Repository {
	return &Repository{
		data:  make(map[string]*model.Vacancy),
		count: 1,
	}
}

func (r *Repository) AddVacancy(vacancy model.Vacancy) error {
	r.Lock()
	defer r.Unlock()

	identifier := vacancy.Identifier.Value
	if _, ok := r.data[identifier]; !ok {
		r.data[identifier] = &vacancy
		r.data[identifier].ID = r.count
		r.count++
		return nil
	}

	return errors.New("vacancy with ID already exists")

}

func (r *Repository) GetByID(id int) (model.Vacancy, error) {
	r.RLock()
	defer r.RUnlock()
	for _, value := range r.data {
		if value.ID == id {
			return *value, nil
		}
	}
	return model.Vacancy{}, errors.New("NotFound")

}

func (r *Repository) GetList() ([]model.Vacancy, error) {
	r.RLock()
	defer r.RUnlock()
	var vacancies []model.Vacancy
	for _, value := range r.data {
		vacancies = append(vacancies, *value)
	}
	return vacancies, nil
}

func (r *Repository) Delete(id int) error {
	r.Lock()
	defer r.Unlock()

	for _, vacancy := range r.data {
		if vacancy.ID == id {
			delete(r.data, vacancy.Identifier.Value)
			return nil
		}
	}
	return errors.New("NotFound")

}
