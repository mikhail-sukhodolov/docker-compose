package main

import (
	"context"
	"github.com/rs/zerolog/log"
	_ "gitlab.com/mikhail-sukhodolov/go-kata/module4/selenium/docs"
	"net/http"
	"os"
	"os/signal"
	"selenium/internal/app/handler"
	"time"
)

// @title           Selenium Parser API
// @version         1.0
// @description     API server for parsing habr vacancies
// @host      localhost:8080

func main() {

	hand := handler.NewHandler()

	port := ":8080"

	server := http.Server{
		Addr:           port,
		Handler:        hand.InitRoutes(),
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	go func() {
		log.Info().Msgf("Server starting at port %v", port)
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatal().Msg("Server stopped")
		}
	}()

	// ожидание сигнала
	quit := make(chan os.Signal, 1)
	// регистрирум обработчик для сигнала os.Interrupt
	signal.Notify(quit, os.Interrupt)
	// блокируем выполнение программы, пока не будет получен сигнал os. Interrupt
	<-quit
	log.Info().Msg("Shutdown Server")

	// тайм-аут завершения
	// создаем контекст ctx с таймаутом в 5 секунд и функцию cancel, которая может быть использована для отмены контекста
	// Если операция не завершится в течение указанного времени, контекст будет отменен и все горутины, связанные с этим контекстом, будут завершены.
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal().Msgf("Server shutdown failed, %v", err)
	}
	defer func(server *http.Server) {
		err := server.Close()
		if err != nil {

		}
	}(&server)

	log.Info().Msg("Server exiting")

}
